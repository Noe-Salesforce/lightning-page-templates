# Custom Lightning Page Template Component Example
This is an example of custom Lightning Page template component. Template uses a Header and three columns with wider center column. Check out the [Lightning component developer guide](https://developer.salesforce.com/docs/atlas.en-us.lightning.meta/lightning/components_config_for_app_builder_template_component.htm) for more information on these types of components.

# Screenshot of this template:
<img width="700" alt="customlayout" src="https://user-images.githubusercontent.com/1404346/34283693-92cfa88c-e710-11e7-9b22-edb35a05a803.png">
